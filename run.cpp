#include <iostream>
#include "Account.cpp"
#include "Accn_List.cpp"
using namespace std;

int main() {
    AccountList account_list ;
    double selected_account;
    Account* manageAccount;
    int program_stat = 0;
    int manageAccountOption;

    cout << "Welcome to Ice Bank What do you want to do" << endl;

    while(program_stat != 7){
        cout << "[1] Add Account" << endl;
        cout << "[2] Manage Account" << endl;
        cout << "[3] Show All Account" << endl;
        cout << "[7] Exit Ice Bank" << endl;

        cout << "$ ";
        cin >> program_stat; 

        if(program_stat == 1){
            account_list.Add_Account();
        }else if(program_stat == 2){

            cout << "Input your account number" << endl;
            cin >> selected_account;
            manageAccount = account_list.findAccount(selected_account);
            if(manageAccount != NULL){
                while(manageAccountOption != 4){
                    cout << "What do you want to do?" << endl;
                    cout << "[1] Deposit Accout" << endl;
                    cout << "[2] Withdraw Accout" << endl;
                    cout << "[3] Checking the balance" << endl;
                    cout << "[4] Exit" << endl;
                    cout << "$ ";
                    cin >> manageAccountOption;

                    if(manageAccountOption == 1){
                        double depositAmount;
                        cout << "How much you want to deposit?" << endl;
                        cout << "$ ";
                        cin >> depositAmount;
                        manageAccount->deposit_account(depositAmount);
                    }else if (manageAccountOption == 2) {
                        double withdrawAmount;
                        cout << "How much you want to withdraw?" << endl;
                        cout << "$ ";
                        cin >> withdrawAmount;
                        manageAccount->withraw_account(withdrawAmount);
                    }else if (manageAccountOption == 3){
                        manageAccount->check_balance();
                    }else {
                        cout << "Command not found please try again" << endl;
                    }
                }
                manageAccountOption = 0;
            }else{
                cout << "Sorry we could not find your account" << endl;
            }

        }else if(program_stat == 3){
            account_list.List_account();
        }else if(program_stat == 7){
            cout << "Bye!!" << endl;
        }else {
            cout << "Sorry we could not find your command" << endl;
        }

    }
    return 0;
}