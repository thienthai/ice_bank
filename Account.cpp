#ifndef ACCOUNT_HEAD
#define ACCOUNT_HEAD

#include <iostream>
using namespace std;

class Account {
    public:
        double account_number;
        double account_balance;
        string account_name;

        void deposit_account(double amount) {
            account_balance = account_balance + amount;
            cout << "You deposit " << amount << " Baht" << endl;
            cout << "Your remaining balance  is " << account_number << " Baht" << endl;
        }

        void withraw_account(double amount){
            account_balance = account_balance - amount;
            cout << "You withraw " << amount << " Baht" << endl;
            cout << "Your remaining balance is " << account_number << " Baht" << endl;
        }

        void check_balance(){
            cout << "Your balance is " << account_balance << " Baht" << endl;          
        }
};

#endif