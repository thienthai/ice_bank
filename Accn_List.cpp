#ifndef ACCOUNT_LIST
#define ACCOUNT_LIST
#define NULL __null


#include "Account.cpp"
#include <vector>
#include <cstddef>
#include <iostream>

class AccountList {
    
    public:
        vector<Account> accountList;

        AccountList() {

        } 

        void Add_Account() {
            double account_number;
            string account_name;
            cout << "Please specify the account number: " << endl;
            cout << "$ ";
            cin >> account_number;
            cout << "Please specify the account name: " << endl;
            cout << "$ ";
            cin >> account_name;

            Account newAccount;
            newAccount.account_number = account_number;
            newAccount.account_balance = 0;
            newAccount.account_name = account_name;

            accountList.push_back(newAccount);

            cout << "Success adding account" << endl;
        }

        void List_account() {

            for (int i = 0; i < accountList.size(); i++) {
                cout << "-----------------------------" << endl;
                cout << "Accout Number -> "<< accountList[i].account_number << endl;
                cout << "Accout Name -> "<< accountList[i].account_name << endl;
                cout << "Accout Balance -> "<< accountList[i].account_balance << endl;
                cout << "-----------------------------" << endl;
            }

        }

        Account* findAccount(double accountNum) {
            for (int i = 0; i < accountList.size(); i++) { 
                if(accountList[i].account_number == accountNum){
                    return &accountList[i];
                }
            }

            return NULL;
        }

};

#endif